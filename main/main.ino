/**
 * Henrique Teixeira Arroyo 
 * INF746 — IoT and Clouds
 * 
 * Final Project - Water Heating System
 */
#include "jsmn.h"
#include "mbedtls-wrapper.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"

#include <rgb_lcd.h>
#include <Servo.h> 
#include <TH02_dev.h> 

#define SIZE 10
#define BUTTON_INPUT 0
#define RELAY 5
#define SERVO 6

#define G_ANGLE -45
#define E_ANGLE 45

rgb_lcd lcd;
Servo servo;

float electric_cost = 1;
float gas_cost = 1;

boolean remote_button = 0;
boolean local_button = 0;

float target_temperature = 0;
float water_temperature = 0;

float moisture_level;

enum STATE {
    OFF='O', 
    GAS_HEATING='G', 
    ELECTRIC_HEATING='E'
} water_heater_state;

void setup() {
  water_heater_state = OFF;
  
  Serial.begin(9600);
  lcd.begin(16, 2);
  
  servo.attach(SERVO);
  servo.write(G_ANGLE);
  
  TH02.begin();
  
  pinMode(RELAY, OUTPUT);
  pinMode(BUTTON_INPUT, INPUT);
}

/**
 * Method will process the command inserted by the user
 */
void process_command(char command, char *value){
  switch(tolower(command)){
    case 'g':
      gas_cost = atof(value);
      break;  
    case 'e':
      electric_cost = atof(value);
      break;
    case 't':
      target_temperature = atof(value);
      break;
    case 'r':
      remote_button = atoi(value) != 0;
      break;
    case 'l':
      local_button = atoi(value) != 0;
      break;
    case 'w':
      water_temperature = atof(value);
      break;
    case 'm':
      moisture_level = atof(value);
      break;
  }
}

/**
 * Function will display the system status in the LCD
 */
void display_status(){ 
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.write(water_heater_state);
  lcd.setCursor(4,0);
  lcd.print(water_temperature); lcd.print(" C");
  lcd.setCursor(4, 1);
  lcd.print(target_temperature); lcd.print(" C");
}

/**
 * Function will set the system on/off according to state parameter (true/false)
 * It will determine which heater to use given the conditions at runtime
 */
void set_heating_state(boolean state){
  if(!state){
    water_heater_state = OFF;
  } else {
    float real_gas_cost = gas_cost * (1.0 - (moisture_level/1000.0)*8.0);
    if(real_gas_cost > electric_cost){
      water_heater_state = ELECTRIC_HEATING;
    } else {
      water_heater_state = GAS_HEATING;
    }
  }
}

/**
 * Function reads all system input
 */
void get_input(){
  local_button = digitalRead(BUTTON_INPUT);
  water_temperature = TH02.ReadTemperature(); 
  moisture_level = TH02.ReadHumidity();
}

/**
 * Function sets all outputs according to system status
 */
void set_output(){
  int angle = water_heater_state == GAS_HEATING ? G_ANGLE : E_ANGLE;
  
  digitalWrite(RELAY, water_heater_state != OFF);
  
  if(water_heater_state == GAS_HEATING){ 
    servo.write(G_ANGLE);
  }
  else if(water_heater_state == ELECTRIC_HEATING){
    servo.write(E_ANGLE);
  }
}

/**
 * Function will check the system status and verify if the heater must be turned on or off
 */
void check_system_status(){
  get_input();
  
  // If the heater is required, turn on the most economic option
  if(remote_button || local_button){
    if(water_temperature < target_temperature){
      set_heating_state(1);
    } else {
      set_heating_state(0);  
    }
  } else {
    // Turn off heater if it is not required
    set_heating_state(0);
  }

  set_output();
}

void loop() {
  char value[SIZE+1];

  while(Serial.available()){
    char command = Serial.read();
    value[SIZE] = '\0';
    for(int index = 0; Serial.available() && index < SIZE; index++){
      value[index] = Serial.read();
      if(value[index] == '\n' || value[index] == ','){
        value[index+1] = '\0';
        break;
      }
    }
    process_command(command, value);
  }

  check_system_status();
  display_status();
  
  delay(1000);
}
